Dear member,

You are invited to participate in our first ideation session for the creation of a brand new digital experience for St John Volunteers. The aims of this session will be to understand the challenges faced by St John and what features we want in a new app that encompasses all St John interactions from how divisions communicate to how we plan events and will act as a single touchpoint for volunteers within our organisation.

 This will be a creative session with some voting and discussion. Because we expect the number of volunteers who engage with this process to be somewhat large and, due to covid restrictions, we'll be forming part of the ideation session as a survey. This survey is open to all volunteers regardless of intention to come to the session. The main session will be run through Microsoft Teams. While the date and time is still flexible, we expect it will be run on the 26th of August at 7pm. More details including an invite will be sent to members who have indicated a desire to participate. Participants must RSVP at least 2 days before the event and will need a personal mobile device in order to vote at the end of the session.

We'd like to remind you that the goal of this session is to be creative and open minded about what could be possible within St John, try to think about things that impact you, but also about ideas that seem wild and out of the box. We promise to consider your ideas seriously, and if they're possible for us to do and fit within the scope of an app or digital experience, we'll do our best to make it happen.


Survey question 1:
What are some of your biggest frustrations when interacting with St John, other members, or with the community. Please try to find at least 3 frustrations and order them from most frustrating to least. You can enter a maximum of 10 frustrations.

Survey question 2:
Now that we've thought about some of the challenges we face, lets come up with some new and positive ideas about how to solve them and how to improve the way we engage as volunteers. 

What's one thing you **need** as a St John Volunteer, this could be something like "a way to talk to my division" or "a clearer way to progress in the organisation"

What's one thing you'd **like** as a St John Volunteer, this could be something like "An easy way to see my duty history" or "A way to get feedback from my division"

What's one **cool** thing you'd like as a St John Volunteer, this could be something way outside the box like "Discount vouchers for completing duties" or "A way to have friendly competition between divisions"

Survey question 3 (optional):
(optional) Do you have any concerns or things you'd like to see during the *process of developing this app*. We can't make guarantees but we'd like to hear your thoughts as even if we can't use your ideas directly, we'll take them onboard and use them to steer our decision making process.

Survey question 4: details.
Name
Email
Rank/Role
Division
Want to attend Y/N






Reinforce that this is a creative session, if you don't want to contribute then you don't show up. This is about coming up with frustrations and how we can solve them, not about airing grievances.

**LMS**The goal is to create a new LMS

**APP**The goal is to create a brand new digital experience for volunteers and redefine the way volunteers interact with St John and the way St John communicates with you. Our goal is to encompass all St John interactions from how divisions communicate to how we plan events.

mandatory RSVP and how they're going to join and you need a personal mobile device
2 day prior cutoff - no fill out, no come -but let people say if they're coming or not and still submit
