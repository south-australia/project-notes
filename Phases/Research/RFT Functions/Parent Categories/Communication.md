This is a linking page to link the functional centre of the App.
This is about functionality and not about UX or Engagement centres.

**Parent**
[[RFT Items]]

**Children**
[[Ability to do short question survey]]
[[Act as a single point of reference for static document resources]]
[[Alerts on emergency response]]
[[!!NOTDEFINITIVE!! - Complete a patient interaction record form, and send it to a central db.]]
[[Do a preliminary risk assessment and send that assessment immediately onward]]
[[Post an Event Commander report]]
[[Provide a feedback form (individual feedback filtered to single source and not visible to all)]]
[[Provide a platform for disseminating communications]]
[[Provide for a rating notification after each Divisional meeting or work week]]
[[Put out a call for volunteers to attend events not yet fully rostered]]
[[Report a safety alert in real time]]
[[Welfare notification (child protection, fraud notification)]]


*GroupingTags*
#parentCategory