This is a linking page to link the functional centre of the App.
This is about functionality and not about UX or Engagement centres.

**Parent**
[[RFT Items]]

**Children**
[[Ability to book training courses]]
[[Ability to do short question survey]]
[[Ability to track individual volunteer hours]]
[[Fundraising progress by Division (eg thermometer of funds raise)]]
[[Map a personal career pathway from various options]]
[[Provide a platform for disseminating communications]]
[[Special offers to members from 3rd parties (eg free coffee at OTR today for St John members)]]
[[Track of individual competencies and accreditation status]]




*GroupingTags*
#parentCategory