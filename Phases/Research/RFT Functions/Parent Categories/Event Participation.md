This is a linking page to link the functional centre of the App.
This is about functionality and not about UX or Engagement centres.

**Parent**
[[RFT Items]]

**Children**
[[Alerts on emergency response]]
[[Check in to an event when you attend]]
[[Check out of an event when you leave]]
[[!!NOTDEFINITIVE!! - Complete a patient interaction record form, and send it to a central db.]]
[[Post an Event Commander report]]
[[Put out a call for volunteers to attend events not yet fully rostered]]
[[Report a safety alert in real time]]
[[Show an Event Commander dashboard (role specific)]]
[[Welfare notification (child protection, fraud notification)]]




*GroupingTags*
#parentCategory