This is a linking page to link the functional centre of the App.
This is about functionality and not about UX or Engagement centres.

**Parent**
[[RFT Items]]

**Children**
[[Book equipment for rental (eg cars, marquees)]]
[[Central control to issue password access and lock out access]]
[[Provide a listing of Division contacts]]
[[Provide group travel arrangements (eg St John “taxi” service to get to events)]]
[[Track of certified drivers license and driver training]]





*GroupingTags*
#parentCategory