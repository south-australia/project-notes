Parent for RFT Items


RFT Functional Categories:
[[Communication]]
[[Event Participation]]
[[Organisation]]
[[Scheduling]]
[[Volunteer Engagement]]

RFT Interaction Centres:
[[Grade 2]]
[[Staff]]
[[Members]]
[[Area Manager]]
[[Grade 1]]



*GroupingTags*
#parentCategory 
#interactionCentre 
#userRoles