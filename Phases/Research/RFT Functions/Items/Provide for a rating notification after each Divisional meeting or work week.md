Each user, receives a pop up question requiring a score response, with the timing of the pop up dictated by the “Division specific meeting schedule”



**META**
*Category*
[[Communication]] #Communication 

*Interaction*
[[Community]]

*Roles*
[[Members]]
[[Area Manager]]
[[Grade 2]]


*Tags*
#VOL_to_DIV_Communication 
#Records 
#Volunteer_Engagement 
#VOL_to_STJ_Communication 



*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 