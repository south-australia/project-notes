Schedule management capability by “super-users”, to be able to set a meeting schedule that populates on different user groups apps


**META**
*Category*
[[Scheduling]] #Scheduling

*Interaction*
[[Planner]]

*Roles*
[[Members]]
[[Area Manager]]

*Tags*
#Division_Schedule
#DIV_to_VOL_Communication
#Volunteer_Scheduling 
#Volunteer_Management 




*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 
