fixed diagram of career pathway at St John. User ability to click on a position and see what they need to do to qualify for the next rank/position


**META**
*Category*
[[Volunteer Engagement]] #Volunteer_Engagement 

*Interaction*
[[Community]]

*Roles*
[[Members]]
[[Area Manager]]
[[Grade 2]]


*Tags*
#Incentive 



*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 