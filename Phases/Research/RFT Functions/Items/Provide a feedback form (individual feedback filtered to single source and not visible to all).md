Simple data entry field, sent to a single source as an email – may want historical reporting


**META**
*Category*
[[Communication]] #Communication 

*Interaction*
[[Community]]

*Roles*
[[Members]]
[[Staff]]


*Tags*
#VOL_to_STJ_Communication 
#Records 



*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 