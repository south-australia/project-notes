Linked to “see what events are coming up” – user can select an event and nominate themselves to attend. This tracks total user nominations to a max capacity and then does not allow over subscription to the event


**META**
*Category*
[[Scheduling]] #Scheduling 

*Interaction*
[[Planner]]

*Roles*


*Tags*
#Volunteer_Scheduling
#Event_Schedule 
#Division_Schedule 




*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 