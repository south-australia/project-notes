Centrally controlled real time push notification to volunteers at an event.


**META**
*Category*
[[Communication]] #Communication 
[[Event Participation]]

*Interaction*
[[Event Actions]]

*Roles*
[[Area Manager]]
[[Members]]
[[Grade 2]]

*Tags*
#STJ_to_VOL_Communication 




*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 