User entry to input their hours on various things; event volunteering, state duties, training, etc


**META**
*Category*
[[Volunteer Engagement]] #Volunteer_Engagement 
[[Scheduling]]

*Interaction*
[[Personnel Records]]

*Roles*
[[Area Manager]]
[[Grade 1]]
[[Grade 2]]
[[Members]]

*Tags*
#Information
#Records 
#Volunteer_Management 
#Volunteer_Scheduling 




*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 