# Items Overview
 
```ccard
type: folder_brief_live
```
 
##RULES:
- Items may have multiple categories
- Items may only have **ONE** Interaction Centre
- Items may have multiple User Roles
- Items have multiple Tags