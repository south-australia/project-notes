Super user controlled. Ability to create and launch a short up to 10 question survey, visible to their Division users only, with reporting capabilities. Survey to disappear for the user once completed, even if survey is open for long time frame (eg weeks)

**META**
*Category*
[[Communication]] #Communication
[[Volunteer Engagement]]

*Interaction*
[[Community]]

*Roles*
[[Area Manager]]
[[Grade 2]]

*Tags*
#VOL_to_DIV_Communication
#Records 
#Volunteer_Engagement 




*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 