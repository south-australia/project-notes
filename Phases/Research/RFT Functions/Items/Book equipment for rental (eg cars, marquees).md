Feeding off of a back office assets register, run a booking system for “renting” equipment and allow the user to “rent” equipment or see if it is already allocated


**META**
*Category*
[[Organisation]] #Organisation

*Interaction*
[[Planner]]

*Roles*
[[Members]]
[[Area Manager]]
[[Staff]]


*Tags*
#Asset_Management
#Records 




*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 