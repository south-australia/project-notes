A central source that can push communications to the users, by way of 1) updating a rolling list of comms 2) when urgent, cause a real time push notification


**META**
*Category*
[[Communication]] #Communication 
[[Volunteer Engagement]]

*Interaction*
[[Community]]

*Roles*
[[Members]]
[[Staff]]
[[Grade 2]]
[[Grade 1]]
[[Area Manager]]


*Tags*
#STJ_to_VOL_Communication 
#Information 
#Volunteer_Engagement 



*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 