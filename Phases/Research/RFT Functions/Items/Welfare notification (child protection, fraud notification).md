Simple user entry form, email straight to predetermined email address. Requires an ability to do historical reporting]


**META**
*Category*
[[Communication]] #Communication 
[[Event Participation]]

*Interaction*
[[Event Actions]]

*Roles*


*Tags*
#VOL_to_STJ_Communication 
#Records 



*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 