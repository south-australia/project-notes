user interacts with a pro forma, completes mandatory fields and sends the report instantly to the central db

 Nb – security/privacy issues with this aspect due to patient confidentiality
 
 
 **META**
*Category*
[[Event Participation]] #Event_Participation 
[[Communication]]

*Interaction*
[[Event Actions]]

*Roles*
[[Members]]
[[Area Manager]]
[[Grade 2]]
[[Grade 1]]
[[Staff]]

*Tags*
#Records 
#Communication 
#VOL_to_STJ_Communication 



*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 