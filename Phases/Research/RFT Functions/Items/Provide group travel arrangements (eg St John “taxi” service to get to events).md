“Super-Users” to be able to set up a simple notification of a group transit event, where users can nominate themselves as a passenger to that event until it is fully booked


**META**
*Category*
[[Organisation]] #Organisation 

*Interaction*
[[Event Actions]]

*Roles*
[[Members]]
[[Area Manager]]


*Tags*
#Asset_Management 
#Volunteer_Scheduling 



*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 