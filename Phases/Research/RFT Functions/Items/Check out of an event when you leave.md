User clicks a button to state they are leaving an event they are rostered for


**META**
*Category*
[[Event Participation]] #Event_Participation 

*Interaction*
[[Event Actions]]

*Roles*
[[Members]]
[[Area Manager]]
[[Grade 2]]

*Related*
[[Show an Event Commander dashboard (role specific)]]

*Tags*
#Records
#Scheduling 
#Event_Schedule 
#Volunteer_Scheduling 
#Division_Schedule 



*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 