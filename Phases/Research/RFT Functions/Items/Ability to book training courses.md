Linked to our training booking system, ability to see available courses, seats available, book themselves in to the course, manage their booking


**META**
*Category*
[[Volunteer Engagement]] #Volunteer_Engagement 

*Interaction*
[[Planner]]

*Roles*
[[Area Manager]]
[[Grade 1]]
[[Grade 2]]
[[Members]]

*Tags*
#Training 
#Salesforce





*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 
