Linked to our bookings system, a console that allows the user to see the upcoming events, filter the events (eg by date)


**META**
*Category*
[[Scheduling]] #Scheduling 

*Interaction*
[[Planner]]

*Roles*


*Tags*
#Event_Schedule
#Volunteer_Scheduling 
#Information 
#Dashboards 



*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 