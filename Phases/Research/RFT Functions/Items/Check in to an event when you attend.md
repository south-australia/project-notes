User clicks a button to state they are at the event they are rostered for


**META**
*Category*
[[Event Participation]] #Event_Participation

*Interaction*
[[Event Actions]]

*Roles*
[[Area Manager]]
[[Members]]
[[Grade 2]]

*Related*
[[Show an Event Commander dashboard (role specific)]]

*Tags*
#Records
#Scheduling 
#Event_Schedule 
#Volunteer_Scheduling 
#Division_Schedule 





*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 