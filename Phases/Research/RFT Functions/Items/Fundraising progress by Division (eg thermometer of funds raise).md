Centrally controlled. Upload of data pertaining to funds by location


**META**
*Category*
[[Volunteer Engagement]] #Volunteer_Engagement 

*Interaction*
[[Community]]

*Roles*
[[Members]]
[[Area Manager]]


*Tags*
#Incentive



*GroupingTags*
#parentCategory 
#interactionCentre
#userRoles 