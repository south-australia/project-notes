## Ideation Planning

#### Communication prior [[Ideation Session Invite]]
Reinforce that this is a creative session, if you don't want to contribute then you don't show up. This is about coming up with frustrations and how we can solve them, not about airing grievances.

**LMS**The goal is to create a new LMS

**APP**The goal is to create a brand new digital experience for volunteers and redefine the way volunteers interact with St John and the way St John communicates with you. Our goal is to encompass all St John interactions from how divisions communicate to how we plan events.

##### Survey:

Email address

((LMS QUESTIONS FIRST))
	What roadblocks do you face when training and upskilling in st john
	What are the 3 features you most want to see in an LMS
	Grouping activity to prioritise
	voting
((ENDOFLMSQUESTIONS))

top 10 frustrations, weighted 1 to 10 where 1 is the most frustrating
What's 1 thing you think would be cool, what's 1 thing you need and what's 1 thing you'd like.
	- The examples need to be creative to prime their brains
Discussions to choose 3 lists, top 3 coolest, top 3 needs, top 3 wants
Voting

mandatory RSVP and how they're going to join and you need a personal mobile device
2 day prior cutoff - no fill out, no come -but let people say if they're coming or not and still submit

#### Session length
~1:30
~30 minutes per Lms/App
~5 minute intro
~5 minute app intro
~5 minute lms intro


#### Session Plan
Intro
Go through the negatives
Introduction to the positives
Breakout into groups to discuss needs/wants
Present their lists
Vote


#### Attendees
Every volunteer if they want to - including comcare, historical
-Need to ask for clarity around including Cadets


#### Activities
- [ ] - Go through the top ten negatives
- [ ] - Explain how we grouped the positive ideas
- [ ] - Breakout session to discuss and choose top 3 need, top 3 want, top 3 coolest
		*If they want groups can add 1 new idea in each category that they come up with together*
- [ ] - Each group gets 2 minutes to present their list
- [ ] - Voting on positive ideas (likely 5 votes each)


#### Action Items / ToDo
- [x] - Ask about including cadets (Ask Kerry)
- [x] - Write out schedule
- [x] - Send out questions
- [ ] - Collate responses and prepare
- [ ] - Test Breakout Groups in teams


#### MetaData
In attendance:
	@Andrew
	@Ryan

